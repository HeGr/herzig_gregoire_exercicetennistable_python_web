#data_gestion_entraineur.py
# GH 2020.04.21 Permet de gérer l'affichage des données de la table t_entraineur

from flask import flash

from Code_Python.APP_EXERCICES.DATABASE import connect_db_context_manager
from Code_Python.APP_EXERCICES import obj_mon_application
from Code_Python.APP_EXERCICES.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from Code_Python.APP_EXERCICES.DATABASE.erreurs import *

class GestionEntraineur():
    def __init__(self):
        try:
            print("dams ée try de gestion entraineur")
            # La connexion à la BD est-elle active
            # Rnevoie une erreur si la connexion est perdue
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Dans Gestion entraineur ...terrible erreur, il faut conencter une base de donnée", "danger")
            # Debug bon marché : Pour afficher un message dans la console
            print(f"Exception grave Classe constructeur GestionEntraineur {erreur.args[0]}")
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

        print("Classe constructeur GestionEntraineur ")

    def entraineur_afficher_data(selfs):
        try:
            # selection des champs à afficher
            # permet de lever une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            strsql_entraineur_afficher = """SELECT id_Entraineur, Nom_Entraineur, 
                                            Prenom_Entraineur, NumeroJS_Entraineur from t_entraineur"""
            # Comme on utilise un context manager, on va accéder au cursor avec with
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # on envoie la commande sql
                mc_afficher.execute(strsql_entraineur_afficher)
                # on récupère les données de la requête
                data_entraineur = mc_afficher.fetchall()
                # affichage dans la console
                print("data_entraineur ",data_entraineur, " Type : ", type(data_entraineur))
                # retourne les données du "SELECT"
                return data_entraineur
        except pymysql.Error as erreur:
            print(f"DGF gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(f"DGG fad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGF gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG fad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # raise MaBdErreurDoublon(f"{msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")
            raise MaBdErreurConnexion(f"DGF fad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

# GH 2020.04.30 Ajout des différentes fonctions CUD.

    def add_entraineur_data(self, valeurs_insertion_dictionnaire):
        try:
            strsql_insert_entraineur = "INSERT  INTO t_entraineur (id_Entraineur, Nom_Entraineur,Prenom_Entraineur,NumeroJS_Entraineur) VALUES (NULL, %(value_nom_entraineur)s,%(value_prenom_entraineur)s,%(value_numeroJS_entraineur)s)"
            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_insert_entraineur,valeurs_insertion_dictionnaire)

        except  pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

    def edit_entraineur_data(self,valeur_id_dictionnaire):
        try:
            print("BUG",valeur_id_dictionnaire)
            #Commande sql pour afficher le entraineur sélectionnée dans le tableau dans le formulaire HTML
            str_sql_id_entraineur = "SELECT id_Entraineur,Nom_Entraineur,Prenom_Entraineur,NumeroJS_Entraineur FROM t_entraineur WHERE id_Entraineur = %(value_id_entraineur)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_id_entraineur, valeur_id_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire", data_one)
                    return data_one
        except Exception as erreur:
            # GH 2020.04.21 MEssage en cas d'échec du bon dérouelement des commandes ci-dessus
            print(f"Problème edit_entraineur_data Data Gestion Entraineur numéro de l'erreur : {erreur}", "danger")
            # On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception...Problème edit_entraineur_data d'un entraineur Data Gestion Entraineur {erreur}")

    def update_entraineur_data(self, valeur_update_dictionnaire):
        try:
            print("DEBUG",valeur_update_dictionnaire)
            # GH 2020.04.21 Commande MySql pour la modification de la valeur tapée au clavier dans le champ "nameEditTypeHTML" du form HTML "type_edit.html"
            # le %s permet d'éviter les injection sql simples

            # commande sql pour afficher le type sélectionné dans le tableau dans le formulaire HTML
            str_sql_update_id_entraineur = "UPDATE t_entraineur SET Nom_Entraineur = %(value_nom_entraineur)s,"\
                                        "Prenom_Entraineur = %(value_prenom_entraineur)s,"\
                                        "NumeroJS_Entraineur = %(value_numeroJS_entraineur)s"\
                                        " WHERE id_Entraineur = %(id_entraineur)s"
            print("str_sql_update_id_entraineur = ",str_sql_update_id_entraineur )

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_update_id_entraineur, valeur_update_dictionnaire)
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Message en cas d'échec du bon déroulement des commandes ci-dessus
            print(f"Problème update_entraineur_data Data Gestion Entraineur numéro de l'erreur : {erreur}")

            if erreur.args[0] == 1062:
                flash(f"Flash. Cette valeur existe déjà : {erreur}", "danger")
                # 2 manière de communiquer une erreur causée par l'insertion d'une valeur à double
                flash("Doublo !! Introduire une valeur différente")
                # Message en cas d'échec du bon déroulement des commandes ci-dessus
                print(f"Problème update_entraineur_data Data Gestion Entraineur numéro de l'erreur : {erreur}")

                raise Exception("Raise exception...Problème update_entraineur_data d'un entraineur DataGestionEntraineur {erreur}")

    def delete_select_entraineur_data(self, valeur_delete_dictionnaire):
        try:
            print("valeur dico BUUUUUUUGGGGGGGEEEEEEERRRRR ",valeur_delete_dictionnaire)
            # OM 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditIntituleGenreHTML" du form HTML "EntraineurEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le genre sélectionné dans le tableau dans le formulaire HTML
            str_sql_select_id_entraineur = "SELECT id_Entraineur,Nom_Entraineur,Prenom_Entraineur,NumeroJS_Entraineur FROM t_entraineur WHERE id_Entraineur = %(value_id_entraineur)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une gméthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_select_id_entraineur, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_select_entraineur_data Gestions Entraineur numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème delete_select_entraineur_data numéro de l'erreur : {erreur}", "danger")
            raise Exception(
                "Raise exception... Problème delete_select_entraineur_data d\'un entraineur Data Gestions Entraineur {erreur}")

    def delete_entraineur_data(self, valeur_delete_dictionnaire):
        try:
            print("valeur dico DEBUUUUGGGGGGGGEEEEEEEEEEEERRRRRRR ", valeur_delete_dictionnaire)
            # GH 2019.04.21 Commande MySql pour EFFACER la valeur sélectionnée par le "bouton" du form HTML "entraineur_edit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            str_sql_delete_nomentraineur = "DELETE FROM t_entraineur WHERE id_Entraineur = %(value_id_entraineur)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_nomentraineur, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_entraineur_data Data Gestions Type numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions Entraineur numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # OM 2020.04.09 Traitement spécifique de l'erreur 1451 Cannot delete or update a parent row: a foreign key constraint fails
                # en MySql le moteur INNODB empêche d'effacer un entraineur qui est associé à un type dans la table intermédiaire "t_avoirtype"
                # il y a une contrainte sur les FK de la table intermédiaire "t_avoirtype"
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                # flash(f"Flash. IMPOSSIBLE d'effacer !!! Cet entraineur est associé à des types dans la t_avoirtype !!! : {erreur}", "danger")
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !!! Cet entraineur est associé à des types dans la t_avoirtype !!! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")
