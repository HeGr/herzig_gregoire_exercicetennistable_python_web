#routes_gestion_entraineur.py
# GH 2020.04.21 Gestion des "routes" FLASK pour les entraineur

import pymysql
from flask import render_template, flash, redirect, url_for, request
from Code_Python.APP_EXERCICES import obj_mon_application
from Code_Python.APP_EXERCICES.ENTRAINEUR.data_gestion_entraineur import GestionEntraineur
from Code_Python.APP_EXERCICES.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from Code_Python.APP_EXERCICES.DATABASE.erreurs import *
import re
# GH 2020.04.21 Afficher un avertissement sympa...mais contraignant
# Pour le tester http://127.0.0.1:1234/avertissement_sympa_pour_geeks

# Afficher les entraineurs
# Pour tester http://127.0.0.1:1234/entraineur_afficher
@obj_mon_application.route("/entraineur_afficher")
def entraineur_afficher():
    # Pour savoir si les données d'un formulaire sont un affichage ou un envoi de données par des champs
    # du formulaire HTML
    if request.method == "GET":
        try:
            # objet contenant toutes les méthodes CRUD des données
            obj_actions_entraineur = GestionEntraineur()
            # récupère les données grâce à une requête MySql définie dans GestionExercice()
            # Fichier data_gestion_entraineur.py
            data_entraineur = obj_actions_entraineur.entraineur_afficher_data()
            # DEBUG bon marché : pour afficher un message dans la console
            print(" data entraineur", data_entraineur," type ",type(data_entraineur))

            # la ligne suivante permet de donner un sentiment rassurant à l'utilisateur
            flash("Données entraineur affichées !!", "Success")
        except Exception as erreur:
            print(f"RGF Erreur générale")
            # On dérive "Exception" par le "@obj_mon_application_errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGF Erreur générale. {erreur}")
    # Envoie la page au serveur
    return render_template("entraineur/entraineur_afficher.html", data=data_entraineur)

# Ajouter les entraineurs
# Pour tester http://127.0.0.1:1234/entraineur_add
@obj_mon_application.route("/entraineur_add", methods=['GET','POST'])
def entraineur_add():
    # on vérifie si les données sont un affichage ou un envoi par un formulaire HTML
    if request.method=='POST':
        try:
            obj_actions_entraineur = GestionEntraineur()

            # Récupération des champs
            name_entraineur = request.form['name_entraineur_html']
            print("nom", type(name_entraineur))
            firstname_entraineur = request.form['firstname_entraineur_html']
            print("firstname type", type(firstname_entraineur))
            JSNumber_entraineur = request.form['JSNumber_entraineur_html']
            print("type date of birth: ",JSNumber_entraineur, type(JSNumber_entraineur))

            # pas de nécessité d'insérer une regex pour les entraineurs
            if not re.match("^([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}$",name_entraineur):
                print("rematch nom")
                if not re.match("^([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}$",firstname_entraineur):
                    print("rematch prénom")
                    flash(f"Une entrée incorrecte pour le nom/prénom de la entraineur. Il ne peut contenir que 2 espaces et ne doit"
                          f"contenir d'accents, de '-' !!!")
                    return render_template("entraineur/entraineur_add.html")

            else:
                print("else entraineur-add")
                # if not re.match("^\d{4}\-\d{2}\-\d{2}$", JSNumber_entraineur):
                #     print("rematch date")
                #     flash(f"Le format de la date est incorrecte.... veuillez l'entrer sous la forme JJ-MM-AAAA!!!!")
                #     return render_template("entraineur/entraineur_add.html")
                # else: print("aucun problème")
                print("else rematch nom-prenom")
                valeurs_insertion_dictionnaire = {"value_nom_entraineur": name_entraineur,
                                                  "value_prenom_entraineur": firstname_entraineur,
                                                  "value_numeroJS_entraineur": JSNumber_entraineur}

                obj_actions_entraineur.add_entraineur_data(valeurs_insertion_dictionnaire)

                # rassurer l'utilisateur
                flash(f'Données insérées !!!', 'Success')
                print(f"Données insérées !!!")
                # on va interpréter la route "entraineur_afficher" car l'utilisateur doit voir
                # le nouvel entraineur qu'il vient d'insérer
                return redirect(url_for('entraineur_afficher'))

        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"RGP pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}")
            raise MonErreur(f"Autre erreur")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except Exception as erreur:
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(
                f"RGP Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    return render_template("entraineur/entraineur_add.html")

# Editer un entraineur
@obj_mon_application.route("/entraineur_edit", methods=['GET','POST'])
def entraineur_edit():
    print("entraineur_edit")
    # les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton edit dans e formulaire "entraineur_afficher.html"
    if request.method=='GET':
        print("if")
        try:
            print("try")
            # Récupérer la valeur de l'id_entraineur
            print("prép récup id")
            id_entraineur_edit = request.values['id_entraineur_edit_html']
            print("id_entraineur_edit", id_entraineur_edit)

            #Dictionnaire et insertion dans la BD
            print("")
            valeur_select_dictionnaire = {"value_id_entraineur": id_entraineur_edit}
            print("dico créé")
            obj_actions_entraineur = GestionEntraineur()
            data_id_entraineur = obj_actions_entraineur.edit_entraineur_data(valeur_select_dictionnaire)
            print("data_id_entraineur : ", data_id_entraineur, " type : ", type(data_id_entraineur))

            #rassurer l'utilisateur
            flash(f'Editer une entraineur')
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    print("before render template")
    return render_template('entraineur/entraineur_edit.html', data=data_id_entraineur)
    print("after render")
# UPDATE un entraineur (après edit)
@obj_mon_application.route("/entraineur_update", methods=['GET','POST'])
def entraineur_update():
    # debug bon marché pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))

    if request.method =='POST':
        try:
            print("request values : ",request.values)
             # Récupère la valeur de l'id_Exercice à updater
            id_entraineur_edit = request.values['id_entraineur_edit_html']

            # récupère le contenu des champs
            name_entraineur = request.form['name_edit_name_entraineur_html']
            firstname_entraineur = request.form['name_edit_firstname_entraineur_html']
            JSNumber_entraineur = request.form['name_edit_JSNumber_entraineur_html']

            # création d'une liste avec le contenu des champs du form "edit_exerice.html
            valeur_edit_list = [{'id_entraineur_edit':id_entraineur_edit,
                                'value_nom_entraineur': name_entraineur,
                                'value_prenom_entraineur': firstname_entraineur,
                                'value_numeroJS_entraineur': JSNumber_entraineur}]
            print("type id :", type(id_entraineur_edit))
            print("name_entraineur :", type(name_entraineur))
            print("firstname_entraineur :", type(firstname_entraineur))
            print("JSNumber_entraineur :", type(JSNumber_entraineur))
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence

            if not re.match("(^([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}$)",name_entraineur):
                if not re.match("(^([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}$)",firstname_entraineur):
                    flash('Une entrée incorrecte...!!! Pas de chiffres, pas de caractères spéciaux, d\'espace à double, de double apostrophe'
                          'de double trait d\'union et ne doit pas être vide !!!', 'Danger')
                    # On doit afficher à nouveau le formulaire "genres_edit.html" à cause des erreurs de "claviotage"
                    # Constitution d'une liste pour que le formulaire d'édition "genres_edit.html" affiche à nouveau
                    # la possibilité de modifier l'entrée
                    # Exemple d'une liste : [{'id_type': 13, 'intitule_type': 'philosophique'}]
                    valeur_edit_list = [{'id_entraineur_edit': id_entraineur_edit,
                                    'value_nom_entraineur': name_entraineur,
                                    'value_prenom_entraineur': firstname_entraineur,
                                    'value_numeroJS_entraineur': JSNumber_entraineur}]

                    # Debug bon marché
                    # Pour afficher le contenu et le type de valeurs passées au formulaire "type_edit.html"
                    print(valeur_edit_list, "type ..", type(valeur_edit_list))
                    return render_template("entraineur/entraineur_edit.html", data=valeur_edit_list)
            else:
                valeur_update_dictionnaire ={'id_entraineur': id_entraineur_edit,
                                            'value_nom_entraineur': name_entraineur,
                                            'value_prenom_entraineur': firstname_entraineur,
                                            'value_numeroJS_entraineur': JSNumber_entraineur}
                obj_actions_entraineur = GestionEntraineur()
                data_id_entraineur = obj_actions_entraineur.update_entraineur_data(valeur_update_dictionnaire)
                print("data_id_entraineur", data_id_entraineur, " type : ", type(data_id_entraineur))
                flash('Editer une entraineur')
                return redirect(url_for('entraineur_afficher'))
        except ( Exception,
                 pymysql.err.OperationalError,
                 pymysql.ProgrammingError,
                 pymysql.InternalError,
                 pymysql.IntegrityError,
                 TypeError) as erreur:

            print(erreur.args)
            flash(f"problème entraineur update {erreur.args[0]}")
            # en cas de problème, mais surtout en cas de non respect
            # des règles REGEX dans le champ "name_edit_intitule_type_html"
            return render_template('entraineur/entraineur_edit.html', data=valeur_edit_list)

    return render_template("entraineur/entraineur_update.html")
# Select delete
@obj_mon_application.route('/entraineur_select_delete', methods=['POST','GET'])
def entraineur_select_delete():
    print("select entraineur_select_delete")
    if request.method == 'GET':
        print("if")
        try:
            print("try")
            print("OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.")
            obj_actions_entraineur = GestionEntraineur()
            # OM 2019.04.04 Récupérer la valeur de "idGenreDeleteHTML" du formulaire html "GenresDelete.html"
            id_entraineur_delete = request.args.get('id_entraineur_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_entraineur": id_entraineur_delete}


            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_entraineur = obj_actions_entraineur.delete_select_entraineur_data(valeur_delete_dictionnaire)
            flash(f"EFFACER et c'est terminé pour cette \"POV\" valeur !!!")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Erreur entraineur_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur entraineur_delete {erreur.args[0], erreur.args[1]}")

    # Envoie la page "HTML" au serveur.
    return render_template('entraineur/entraineur_delete.html',data=data_id_entraineur)
# Delete entraineur
@obj_mon_application.route('/entraineur_delete', methods=['POST','GET'])
def entraineur_delete():
    # OM 2019.04.02 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_entraineur = GestionEntraineur()
            # OM 2019.04.02 Récupérer la valeur de "id_TypeExercice" du formulaire html "TypeAfficher.html"
            id_entraineur_delete = request.form['id_entraineur_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_entraineur": id_entraineur_delete}
            print("before delete")
            data_entraineur = obj_actions_entraineur.delete_entraineur_data(valeur_delete_dictionnaire)
            # OM 2019.04.02 On va afficher la liste des type qui sont des personnes
            # OM 2019.04.02 Envoie la page "HTML" au serveur. On passe un message d'information dans "message_html"
            print("after delete")
            # On affiche les type
            return redirect(url_for('entraineur_afficher'))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.04.09 Traiter spécifiquement l'erreur MySql 1451
            # Cette erreur 1451, signifie qu'on veut effacer un "type" qui sont des personnes' qui est associé dans "t_avoirType".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE d\'effacer !!! Cette valeur est associée à des entraineur !')
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !! Cette entraineur est associé à des types dans la t_avoirLicence !!! : {erreur}")
                # Afficher la liste des genres des films
                return redirect(url_for('entraineur_afficher'))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"Erreur entraineur_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur entraineur_delete {erreur.args[0], erreur.args[1]}")
    return render_template('entraineur/entraineur_afficher.html', data=data_entraineur)

