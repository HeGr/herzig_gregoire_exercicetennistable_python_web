#routes_gestion_exercice.py
# GH 2020.04.21 Gestion des "routes" FLASK pour les exercices

import pymysql
from flask import render_template, flash, redirect, url_for, request
from Code_Python.APP_EXERCICES import obj_mon_application
from Code_Python.APP_EXERCICES.MATERIEL.data_gestion_materiel import GestionMateriel
from Code_Python.APP_EXERCICES.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from Code_Python.APP_EXERCICES.DATABASE.erreurs import *
import re
# # GH 2020.04.21 Afficher un avertissement sympa...mais contraignant
# # Pour le tester http://127.0.0.1:1234/avertissement_sympa_pour_geeks
# @obj_mon_application.route("/avertissement_sympa_pour_geeks")
# def avertissement_sympa_pour_geeks():
#     # envoie la page HTML au serveur
#     return render_template("exercice/AVERTISSEMENT_SYMPA_POUR_LES_GEEKS_exercice.html")
#
#
#
#
# Afficher les exercices
# Pour tester http://127.0.0.1:1234/materiel_afficher
@obj_mon_application.route("/materiel_afficher")
def materiel_afficher():
    # Pour savoir si les données d'un formulaire sont un affichage ou un envoi de données par des champs
    # du formulaire HTML
    if request.method == "GET":
        try:
            # objet contenant toutes les méthodes CRUD des données
            obj_actions_materiel = GestionMateriel()
            # récupère les données grâce à une requête MySql définie dans GestionMateriel()
            # Fichier data_gestion_exercice.py
            data_materiel = obj_actions_materiel.materiel_afficher_data()
            # DEBUG bon marché : pour afficher un message dans la console
            print(" data exercice", data_materiel," type ",type(data_materiel))

            # la ligne suivante permet de donner un sentiment rassurant à l'utilisateur
            flash("Données exercice affichées !!", "Success")
        except Exception as erreur:
            print(f"RGF Erreur générale")
            # On dérive "Exception" par le "@obj_mon_application_errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGF Erreur générale. {erreur}")
    # Envoie la page au serveur
    return render_template("materiel/materiel_afficher.html", data=data_materiel)

# Ajouter les exercices
# Pour tester http://127.0.0.1:1234/materiel_add
@obj_mon_application.route("/materiel_add", methods=['GET','POST'])
def materiel_add():
    # on vérifie si les données sont un affichage ou un envoi par un formulaire HTML
    if request.method=="POST":
        try:
            obj_actions_materiel = GestionMateriel()

            # Récupération des champs
            matos_materiel = request.form['matos_materiel_html']

            # pas de nécessité d'insérer une regex pour les exercices
            if not re.match("(^([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}$)",matos_materiel):
                flash(f"Une entrée incorrecte pour le nom de l'exercice. Il ne peut contenir que 2 espaces et ne doit"
                      f"contenir d'accents, de '-' !!!")
                return render_template("materiel/materiel_add.html")
            else:
                valeurs_insertion_dictionnaire = {"value_nom_materiel": matos_materiel}

                obj_actions_materiel.add_materiel_data(valeurs_insertion_dictionnaire)

                # rassurer l'utilisateur
                flash(f'Données insérées !!!', 'Success')
                print(f"Données insérées !!!")
                # on va interpréter la route "exercice_afficher" car l'utilisateur doit voir
                # le nouvel exercice qu'il vient d'insérer
                return redirect(url_for('materiel_afficher'))

        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"RGT pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}")
            raise MonErreur(f"Autre erreur")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except Exception as erreur:
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(
                f"RGT Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    return render_template("materiel/materiel_add.html")

# Editer un exercice
@obj_mon_application.route("/materiel_edit", methods=['GET','POST'])
def materiel_edit():
    print("exercice_edit")
    # les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton edit dans e formulaire "exercice_afficher.html"
    if request.method=='GET':
        print("if")
        try:
            print("try")
            # Récupérer la valeur de l'id_exercice
            print("prép récup id")
            id_materiel_edit = request.values['id_materiel_edit_html']
            print("id_exercice_edit", id_materiel_edit)

            #Dictionnaire et insertion dans la BD
            print("")
            valeur_select_dictionnaire = {"value_id_materiel": id_materiel_edit}
            print("dico créé")
            obj_actions_materiel = GestionMateriel()
            data_id_materiel = obj_actions_materiel.edit_materiel_data(valeur_select_dictionnaire)
            print("dataIDExercice : ", data_id_materiel, " type : ", type(data_id_materiel))

            #rassurer l'utilisateur
            flash(f'Editer un exercice')
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    print("before render template")
    return render_template('materiel/materiel_edit.html', data=data_id_materiel)
    print("after render")
# UPDATE un exercice (après edit)
@obj_mon_application.route("/materiel_update", methods=['GET','POST'])
def materiel_update():
    # debug bon marché pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))

    if request.method =='POST':
        try:
            print("request values : ",request.values)
             # Récupère la valeur de l'id_Exercice à updater
            id_materiel_edit = request.values['id_materiel_edit_html']

            # récupère le contenu des champs
            matos_materiel = request.values['name_edit_nom_materiel_html']

            # création d'une liste avec le contenu des champs du form "edit_exerice.html
            valeur_edit_list = [{'id_Materiel': id_materiel_edit,
                                 'nom_materiel': matos_materiel}]
            print("type id :", type(id_materiel_edit))
            print("nom_materiel :", type(matos_materiel))
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence
            if not re.match("(^([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}$)",matos_materiel):
                flash('Une entrée incorrecte...!!! Pas de chiffres, pas de caractères spéciaux, d\'espace à double, de double apostrophe'
                      'de double trait d\'union et ne doit pas être vide !!!', 'Danger')
                # On doit afficher à nouveau le formulaire "genres_edit.html" à cause des erreurs de "claviotage"
                # Constitution d'une liste pour que le formulaire d'édition "genres_edit.html" affiche à nouveau
                # la possibilité de modifier l'entrée
                # Exemple d'une liste : [{'id_type': 13, 'intitule_type': 'philosophique'}]
                valeur_edit_list = [{'id_Materiel': id_materiel_edit,
                                     'nom_materiel': matos_materiel}]

                # Debug bon marché
                # Pour afficher le contenu et le type de valeurs passées au formulaire "type_edit.html"
                print(valeur_edit_list, "type ..", type(valeur_edit_list))
                return render_template("materiel/materiel_edit.html", data=valeur_edit_list)
            else:
                valeur_update_dictionnaire ={'id_Materiel': id_materiel_edit,
                                            'nom_materiel': matos_materiel}
                obj_actions_materiel = GestionMateriel()
                data_id_materiel = obj_actions_materiel.update_materiel_data(valeur_update_dictionnaire)
                print("data_id_materiel", data_id_materiel, " type : ", type(data_id_materiel))
                flash('Editer un matériel')
                return redirect(url_for('materiel_afficher'))
        except ( Exception,
                 pymysql.err.OperationalError,
                 pymysql.ProgrammingError,
                 pymysql.InternalError,
                 pymysql.IntegrityError,
                 TypeError) as erreur:

            print(erreur.args)
            flash(f"problème type update {erreur.args[0]}")
            # en cas de problème, mais surtout en cas de non respect
            # des règles REGEX dans le champ "name_edit_intitule_type_html"
            return render_template('materiel/materiel_edit.html', data=valeur_edit_list)

    return render_template("materiel/materiel_update.html")
# Select delete
@obj_mon_application.route('/materiel_select_delete', methods=['POST','GET'])
def materiel_select_delete():
    print("select delete")
    if request.method == 'GET':
        print("if")
        try:
            print("try")
            print("OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.")
            obj_actions_materiel = GestionMateriel()
            # OM 2019.04.04 Récupérer la valeur de "idGenreDeleteHTML" du formulaire html "GenresDelete.html"
            id_materiel_delete = request.args.get('id_materiel_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_materiel": id_materiel_delete}


            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_materiel = obj_actions_materiel.delete_select_materiel_data(valeur_delete_dictionnaire)
            flash(f"EFFACER et c'est terminé pour cette \"POV\" valeur !!!")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Erreur genres_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur genres_delete {erreur.args[0], erreur.args[1]}")

    # Envoie la page "HTML" au serveur.
    return render_template('materiel/materiel_delete.html',data=data_id_materiel)
# Delete exercice
@obj_mon_application.route('/materiel_delete', methods=['POST','GET'])
def materiel_delete():
    # OM 2019.04.02 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_materiel = GestionMateriel()
            # OM 2019.04.02 Récupérer la valeur de "id_TypeExercice" du formulaire html "TypeAfficher.html"
            id_materiel_delete = request.form['id_materiel_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_materiel": id_materiel_delete}
            print("before delete")
            data_materiel = obj_actions_materiel.delete_materiel_data(valeur_delete_dictionnaire)
            # OM 2019.04.02 On va afficher la liste des type d'exercice
            # OM 2019.04.02 Envoie la page "HTML" au serveur. On passe un message d'information dans "message_html"
            print("after delete")
            # On affiche les type
            return redirect(url_for('materiel_afficher'))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.04.09 Traiter spécifiquement l'erreur MySql 1451
            # Cette erreur 1451, signifie qu'on veut effacer un "type" d'exercice' qui est associé dans "t_avoirType".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE d\'effacer !!! Cette valeur est associée à des types !')
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !! Ce exercice est associé à des types dans la t_avoirType !!! : {erreur}")
                # Afficher la liste des genres des films
                return redirect(url_for('materiel_afficher'))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"Erreur type_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur type_delete {erreur.args[0], erreur.args[1]}")
    return render_template('materiel/materiel_afficher.html', data=data_materiel)

