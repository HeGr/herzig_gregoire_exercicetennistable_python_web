#routes_gestion_personne.py
# GH 2020.04.21 Gestion des "routes" FLASK pour les personne

import pymysql
from flask import render_template, flash, redirect, url_for, request
from Code_Python.APP_EXERCICES import obj_mon_application
from Code_Python.APP_EXERCICES.PERSONNE.data_gestion_personne import GestionPersonne
from Code_Python.APP_EXERCICES.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from Code_Python.APP_EXERCICES.DATABASE.erreurs import *
import re
# GH 2020.04.21 Afficher un avertissement sympa...mais contraignant
# Pour le tester http://127.0.0.1:1234/avertissement_sympa_pour_geeks

# Afficher les exercices
# Pour tester http://127.0.0.1:1234/exercice_afficher
@obj_mon_application.route("/personne_afficher")
def personne_afficher():
    # Pour savoir si les données d'un formulaire sont un affichage ou un envoi de données par des champs
    # du formulaire HTML
    if request.method == "GET":
        try:
            # objet contenant toutes les méthodes CRUD des données
            obj_actions_personne = GestionPersonne()
            # récupère les données grâce à une requête MySql définie dans GestionExercice()
            # Fichier data_gestion_exercice.py
            data_personne = obj_actions_personne.personne_afficher_data()
            # DEBUG bon marché : pour afficher un message dans la console
            print(" data exercice", data_personne," type ",type(data_personne))

            # la ligne suivante permet de donner un sentiment rassurant à l'utilisateur
            flash("Données exercice affichées !!", "Success")
        except Exception as erreur:
            print(f"RGF Erreur générale")
            # On dérive "Exception" par le "@obj_mon_application_errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGF Erreur générale. {erreur}")
    # Envoie la page au serveur
    return render_template("personne/personne_afficher.html", data=data_personne)

# Ajouter les exercices
# Pour tester http://127.0.0.1:1234/exercice_add
@obj_mon_application.route("/personne_add", methods=['GET','POST'])
def personne_add():
    # on vérifie si les données sont un affichage ou un envoi par un formulaire HTML
    if request.method=='POST':
        try:
            obj_actions_personne = GestionPersonne()

            # Récupération des champs
            name_personne = request.form['name_personne_html']
            print("nom", type(name_personne))
            firstname_personne = request.form['firstname_personne_html']
            print("firstname type", type(firstname_personne))
            dateOfBirth_personne = request.form['dateOfBirth_personne_html']
            print("type date of birth: ",dateOfBirth_personne, type(dateOfBirth_personne))

            # pas de nécessité d'insérer une regex pour les exercices
            if not re.match("^([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}$",name_personne):
                print("rematch nom")
                if not re.match("^([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}$",firstname_personne):
                    print("rematch prénom")
                    flash(f"Une entrée incorrecte pour le nom/prénom de la personne. Il ne peut contenir que 2 espaces et ne doit"
                          f"contenir d'accents, de '-' !!!")
                    return render_template("personne/personne_add.html")

            else:
                print("else personne-add")
                if not re.match("^\d{4}\-\d{2}\-\d{2}$", dateOfBirth_personne):
                    print("rematch date")
                    flash(f"Le format de la date est incorrecte.... veuillez l'entrer sous la forme JJ-MM-AAAA!!!!")
                    return render_template("personne/personne_add.html")
                else: print("aucun problème")
                print("else rematch nom-prenom")
                valeurs_insertion_dictionnaire = {"value_nom_personne": name_personne,
                                                  "value_prenom_personne": firstname_personne,
                                                  "value_dateNaissance_personne": dateOfBirth_personne}

                obj_actions_personne.add_personne_data(valeurs_insertion_dictionnaire)

                # rassurer l'utilisateur
                flash(f'Données insérées !!!', 'Success')
                print(f"Données insérées !!!")
                # on va interpréter la route "exercice_afficher" car l'utilisateur doit voir
                # le nouvel exercice qu'il vient d'insérer
                return redirect(url_for('personne_afficher'))

        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"RGP pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}")
            raise MonErreur(f"Autre erreur")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except Exception as erreur:
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(
                f"RGP Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    return render_template("personne/personne_add.html")

# Editer un exercice
@obj_mon_application.route("/personne_edit", methods=['GET','POST'])
def personne_edit():
    print("personne_edit")
    # les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton edit dans e formulaire "exercice_afficher.html"
    if request.method=='GET':
        print("if")
        try:
            print("try")
            # Récupérer la valeur de l'id_exercice
            print("prép récup id")
            id_personne_edit = request.values['id_personne_edit_html']
            print("id_exercice_edit", id_personne_edit)

            #Dictionnaire et insertion dans la BD
            print("")
            valeur_select_dictionnaire = {"value_id_personne": id_personne_edit}
            print("dico créé")
            obj_actions_personne = GestionPersonne()
            data_id_personne = obj_actions_personne.edit_personne_data(valeur_select_dictionnaire)
            print("data_id_personne : ", data_id_personne, " type : ", type(data_id_personne))

            #rassurer l'utilisateur
            flash(f'Editer une personne')
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    print("before render template")
    return render_template('personne/personne_edit.html', data=data_id_personne)
    print("after render")
# UPDATE un exercice (après edit)
@obj_mon_application.route("/personne_update", methods=['GET','POST'])
def personne_update():
    # debug bon marché pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))

    if request.method =='POST':
        try:
            print("request values : ",request.values)
             # Récupère la valeur de l'id_Exercice à updater
            id_personne_edit = request.values['id_personne_edit_html']

            # récupère le contenu des champs
            name_personne = request.form['name_edit_name_personne_html']
            firstname_personne = request.form['name_edit_firstname_personne_html']
            dateOfBirth_personne = request.form['name_edit_dateOfBirth_personne_html']

            # création d'une liste avec le contenu des champs du form "edit_exerice.html
            valeur_edit_list = [{'id_personne_edit':id_personne_edit,
                                'value_nom_personne': name_personne,
                                'value_prenom_personne': firstname_personne,
                                'value_dateNaissance_personne': dateOfBirth_personne}]
            print("type id :", type(id_personne_edit))
            print("name_personne :", type(name_personne))
            print("firstname_personne :", type(firstname_personne))
            print("dateOfBirth_personne :", type(dateOfBirth_personne))
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence

            if not re.match("(^([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}$)",name_personne):
                if not re.match("(^([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}[ ]?([a-zA-Z]*|[a-zA-Z0-9]*){1}$)",firstname_personne):
                    flash('Une entrée incorrecte...!!! Pas de chiffres, pas de caractères spéciaux, d\'espace à double, de double apostrophe'
                          'de double trait d\'union et ne doit pas être vide !!!', 'Danger')
                    # On doit afficher à nouveau le formulaire "genres_edit.html" à cause des erreurs de "claviotage"
                    # Constitution d'une liste pour que le formulaire d'édition "genres_edit.html" affiche à nouveau
                    # la possibilité de modifier l'entrée
                    # Exemple d'une liste : [{'id_type': 13, 'intitule_type': 'philosophique'}]
                    valeur_edit_list = [{'id_personne_edit': id_personne_edit,
                                    'value_nom_personne': name_personne,
                                    'value_prenom_personne': firstname_personne,
                                    'value_dateNaissance_personne': dateOfBirth_personne}]

                    # Debug bon marché
                    # Pour afficher le contenu et le type de valeurs passées au formulaire "type_edit.html"
                    print(valeur_edit_list, "type ..", type(valeur_edit_list))
                    return render_template("personne/personne_edit.html", data=valeur_edit_list)
            else:
                valeur_update_dictionnaire ={'id_personne': id_personne_edit,
                                            'value_nom_personne': name_personne,
                                            'value_prenom_personne': firstname_personne,
                                            'value_dateNaissance_personne': dateOfBirth_personne}
                obj_actions_personne = GestionPersonne()
                data_id_personne = obj_actions_personne.update_personne_data(valeur_update_dictionnaire)
                print("data_id_personne", data_id_personne, " type : ", type(data_id_personne))
                flash('Editer une personne')
                return redirect(url_for('personne_afficher'))
        except ( Exception,
                 pymysql.err.OperationalError,
                 pymysql.ProgrammingError,
                 pymysql.InternalError,
                 pymysql.IntegrityError,
                 TypeError) as erreur:

            print(erreur.args)
            flash(f"problème personne update {erreur.args[0]}")
            # en cas de problème, mais surtout en cas de non respect
            # des règles REGEX dans le champ "name_edit_intitule_type_html"
            return render_template('personne/personne_edit.html', data=valeur_edit_list)

    return render_template("personne/personne_update.html")
# Select delete
@obj_mon_application.route('/personne_select_delete', methods=['POST','GET'])
def personne_select_delete():
    print("select personne_select_delete")
    if request.method == 'GET':
        print("if")
        try:
            print("try")
            print("OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.")
            obj_actions_personne = GestionPersonne()
            # OM 2019.04.04 Récupérer la valeur de "idGenreDeleteHTML" du formulaire html "GenresDelete.html"
            id_personne_delete = request.args.get('id_personne_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_personne": id_personne_delete}


            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_personne = obj_actions_personne.delete_select_personne_data(valeur_delete_dictionnaire)
            flash(f"EFFACER et c'est terminé pour cette \"POV\" valeur !!!")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Erreur personne_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur personne_delete {erreur.args[0], erreur.args[1]}")

    # Envoie la page "HTML" au serveur.
    return render_template('personne/personne_delete.html',data=data_id_personne)
# Delete exercice
@obj_mon_application.route('/personne_delete', methods=['POST','GET'])
def personne_delete():
    # OM 2019.04.02 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_personne = GestionPersonne()
            # OM 2019.04.02 Récupérer la valeur de "id_TypeExercice" du formulaire html "TypeAfficher.html"
            id_personne_delete = request.form['id_personne_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_personne": id_personne_delete}
            print("before delete")
            data_personne = obj_actions_personne.delete_personne_data(valeur_delete_dictionnaire)
            # OM 2019.04.02 On va afficher la liste des type d'exercice
            # OM 2019.04.02 Envoie la page "HTML" au serveur. On passe un message d'information dans "message_html"
            print("after delete")
            # On affiche les type
            return redirect(url_for('personne_afficher'))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.04.09 Traiter spécifiquement l'erreur MySql 1451
            # Cette erreur 1451, signifie qu'on veut effacer un "type" d'exercice' qui est associé dans "t_avoirType".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE d\'effacer !!! Cette valeur est associée à des entraineur !')
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !! Cette personne est associé à des types dans la t_avoirLicence !!! : {erreur}")
                # Afficher la liste des genres des films
                return redirect(url_for('personne_afficher'))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"Erreur personne_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur personne_delete {erreur.args[0], erreur.args[1]}")
    return render_template('personne/personne_afficher.html', data=data_personne)

