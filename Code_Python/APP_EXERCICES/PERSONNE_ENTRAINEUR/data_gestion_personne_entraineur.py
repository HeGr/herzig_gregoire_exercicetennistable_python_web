#data_gestion_personne_entraineur.py
# GH 17.060.2020, Ce fichier va permettre de gérer les actions CRUD pour la gestion du matériel et des exentraineur

from typing import Any, Union, Tuple

from flask import flash
from Code_Python.APP_EXERCICES.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from Code_Python.APP_EXERCICES.DATABASE.erreurs import *

class GestionPersonneEntraineur():
    def __init__(self):
        try:
            print("dans le try de gestion type entraineur")
            # la connexion à la BD est-elle active
            # Sinon renvoie une erreur
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Dans Gestion Type Entraineur ... terrible errueur, il faut connecter une base de donnée", "danger")
            print(f"Exception grave Classe consturcteur GestionTypeEntraineur {erreur.args[0]}")
            # erreur personnalisée
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")
        print("Classe constructeur GestionTypeEntraineur")
    def personne_afficher_data(self):
        try:

            # préciser les champs que l'on veut afficher
            print("before strsql_personne_afficher")
            strsql_personne_afficher = """SELECT id_personne, nom_personne, prenom_personne, dateNaissance_personne FROM t_personne ORDER BY id_personne ASC"""

            print("request sql", strsql_personne_afficher)

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_personne_afficher)
                # Récupère les données de la requête
                data_nom_ = mc_afficher.fetchall()
                #Affiche dans la console
                print("data_nom_ : ",data_nom_, " Type : ", type(data_nom_))
                # retourne les données du SELECT
                return data_nom_
        except pymysql.Error as erreur:
            print(f"DGTE gad pymysql error {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(f"DGTE gad pymysql error {msg_erreurs['ErreurPyMySql']['message']}{erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGTE gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGTE gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")
    def personne_entraineur_afficher_data (self, valeur_id_personne_selected_dict):
        print("valeur_id_film_selected_dict...", valeur_id_personne_selected_dict)
        try:

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_genres"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher

            # strsql_entraineur_selected = """SELECT id_film, nom_film, duree_film, description_film, cover_link_film, date_sortie_film, GROUP_CONCAT(id_genre) as GenresFilms FROM t_genres_films AS T1
            #                             INNER JOIN t_films AS T2 ON T2.id_film = T1.fk_film
            #                             INNER JOIN t_genres AS T3 ON T3.id_genre = T1.fk_genre
            #                             WHERE id_film = %(value_id_film_selected)s"""
            # strsql_entraineur_selected = """SELECT id_Entraineur, Prenom_Entraineur, Nom_Entraineur, NumeroJS_Entraineur, GROUP_CONCAT(id_personne) as Personne FROM t_avoirlicencejs AS T1
            #                             INNER JOIN t_entraineur AS T2 on T2.id_Entraineur = T1.FK_Entraineur
            #                             INNER JOIN t_personne AS T3 ON T3.id_personne = T1.FK_Personne
            #                             WHERE id_Entraineur = %(value_id_personne_selected)s"""
            strsql_personne_selected = """SELECT id_personne, prenom_personne, nom_personne, dateNaissance_personne, GROUP_CONCAT(NumeroJS_Entraineur) as Entraineur from t_avoirlicenceJS AS t1
                                        INNER JOIN t_entraineur as T2 on T2.id_Entraineur = T1.FK_Entraineur
                                        INNER JOIN t_personne as T3 on T3.id_personne = T1.FK_Personne
                                        WHERE id_personne = %(value_id_personne_selected)s"""
            # strsql_type_entraineur_non_attribues = """SELECT id_genre, intitule_genre FROM t_genres
            #                                         WHERE id_genre not in(SELECT id_genre as idGenresFilms FROM t_genres_films AS T1
            #                                         INNER JOIN t_films AS T2 ON T2.id_film = T1.fk_film
            #                                         INNER JOIN t_genres AS T3 ON T3.id_genre = T1.fk_genre
            #                                         WHERE id_film = %(value_id_film_selected)s)"""
            # strsql_type_entraineur_non_attribues = """SELECT id_TypeEntraineur, Type_TypeEntraineur from t_typeentraineur
            #                                         WHERE id_TypeEntraineur not in(SELECT id_TypeEntraineur as idTypeEntraineur from t_avoirtype as T1
            #                                         INNER JOIN t_entraineur AS T2 on T2.id_Entraineur = T1.FK_entraineur
            #                                         INNER JOIN t_typeentraineur AS T3 ON T3.id_TypeEntraineur = T1.FK_type
            #                                         WHERE id_Entraineur = %(value_id_personne_selected)s)"""
            # """SELECT id_personne, nom_personne, prenom_personne from t_personne
            # WHERE id_personne not in (SELECT id_personne as idpersonneEntraineur from t_avoirlicencejs as T1
            # INNER JOIN t_entraineur AS T2 on T2.id_Entraineur = T1.FK_Entraineur
            # INNER JOIN t_personne AS T3 ON T3.id_personne = T1.FK_Personne
            # WHERE id_Entraineur =%(value_id_personne_selected)s)"""
            strsql_personne_entraineur_non_attribues ="""SELECT id_Entraineur, Nom_Entraineur, Prenom_Entraineur, NumeroJS_Entraineur from t_entraineur
                                                        WHERE id_Entraineur not in (SELECT id_Entraineur as idpersonneEntraineur from t_avoirlicencejs as T1
                                                        INNER JOIN t_personne AS T2 on T2.id_personne = T1.FK_Personne
                                                        INNER JOIN t_entraineur AS T3 ON T3.id_Entraineur = T1.FK_Entraineur
                                                        WHERE id_personne =%(value_id_personne_selected)s)"""

            # strsql_type_entraineur_attribues = """SELECT id_film, id_genre, intitule_genre FROM t_genres_films AS T1
            #                                 INNER JOIN t_films AS T2 ON T2.id_film = T1.fk_film
            #                                 INNER JOIN t_genres AS T3 ON T3.id_genre = T1.fk_genre
            #                                 WHERE id_film = %(value_id_film_selected)s"""
            # strsql_type_entraineur_attribues = """SELECT id_TypeEntraineur, Type_TypeEntraineur from t_avoirtype as T1
            #                                     INNER JOIN t_entraineur AS T2 on T2.id_Entraineur = T1.FK_entraineur
            #                                     INNER JOIN t_typeentraineur AS T3 ON T3.id_TypeEntraineur = T1.FK_type
            #                                     WHERE id_Entraineur = %(value_id_personne_selected)s"""
            strsql_personne_entraineur_attribues = """SELECT id_Entraineur, Nom_Entraineur, Prenom_Entraineur, NumeroJS_Entraineur from t_avoirlicencejs AS T1
                                                    INNER JOIN t_entraineur AS T2 on T2.id_Entraineur = T1.FK_Entraineur
                                                    INNER JOIN t_personne AS T3 ON T3.id_personne = T1.FK_Personne
                                                    WHERE id_personne = %(value_id_personne_selected)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_personne_entraineur_non_attribues, valeur_id_personne_selected_dict)
                # Récupère les données de la requête.
                data_personne_entraineur_non_attribues = mc_afficher.fetchall()
                # Affichage dans la console
                print("dfad data_personne_entraineur_non_attribues ", data_personne_entraineur_non_attribues, " Type : ",
                      type(data_personne_entraineur_non_attribues))

                # Envoi de la commande MySql
                mc_afficher.execute(strsql_personne_selected, valeur_id_personne_selected_dict)
                # Récupère les données de la requête.
                data_entraineur_selected = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_entraineur_selected  ", data_entraineur_selected, " Type : ", type(data_entraineur_selected))

                # Envoi de la commande MySql
                mc_afficher.execute(strsql_personne_entraineur_attribues, valeur_id_personne_selected_dict)
                # Récupère les données de la requête.
                data_personne_entraineur_attribues = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_personne_entraineur_attribues ", data_personne_entraineur_attribues, " Type : ",
                      type(data_personne_entraineur_attribues))

                # Retourne les données du "SELECT"
                return data_entraineur_selected, data_personne_entraineur_non_attribues, data_personne_entraineur_attribues
        except pymysql.Error as erreur:
            print(f"DGGF gfad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGGF gfad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGGF gfad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def personne_entraineur_afficher_data_concat (self, id_personne_selected):
        print("id_personne_selected  ", id_personne_selected)
        try:
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_genres"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher

            # strsql_type_entraineur_afficher_data_concat ="""SELECT id_film, nom_film, duree_film, description_film, cover_link_film, date_sortie_film,
            #                                                 GROUP_CONCAT(intitule_genre) as GenresFilms FROM t_genres_films AS T1
            #                                                 RIGHT JOIN t_films AS T2 ON T2.id_film = T1.fk_film
            #                                                 LEFT JOIN t_genres AS T3 ON T3.id_genre = T1.fk_genre
            #                                                 GROUP BY id_film"""
            # strsql_type_entraineur_afficher_data_concat ="""SELECT id_Entraineur, Visuel_Entraineur, Nom_Entraineur, Description_Entraineur,
            #                                             GROUP_CONCAT(Type_TypeEntraineur) as TypeEntraineur FROM t_avoirtype AS T1
            #                                             RIGHT JOIN t_entraineur AS T2 on T2.id_Entraineur = T1.FK_entraineur
            #                                             LEFT JOIN t_typeentraineur AS T3 ON T3.id_TypeEntraineur = T1.FK_type
            #                                             GROUP BY id_Entraineur"""
            strsql_personne_entraineur_afficher_data_concat = """SELECT id_personne, prenom_personne, nom_personne, dateNaissance_personne, 
                                                            GROUP_CONCAT(NumeroJS_Entraineur) as personneEntraineur FROM t_avoirlicencejs AS T1
                                                            RIGHT JOIN t_entraineur AS T2 on T2.id_Entraineur = T1.FK_Entraineur
                                                            LEFT JOIN t_personne  as T3 on T3.id_personne = T1.FK_Personne
                                                            GROUP BY id_personne"""

          # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # le paramètre 0 permet d'afficher tous les films
                # Sinon le paramètre représente la valeur de l'id du film
                if id_personne_selected == 0:
                    mc_afficher.execute(strsql_personne_entraineur_afficher_data_concat)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                    valeur_id_personne_selected_dictionnaire = {"value_id_personne_selected": id_personne_selected}
                    strsql_personne_entraineur_afficher_data_concat += """ HAVING id_entraineur= %(value_id_personne_selected)s"""
                    # Envoi de la commande MySql
                    mc_afficher.execute(strsql_personne_entraineur_afficher_data_concat, valeur_id_personne_selected_dictionnaire)

                # Récupère les données de la requête.
                data_personne_entraineur_afficher_concat = mc_afficher.fetchall()
                # Affichage dans la console
                print("dgte data_personne_entraineur_afficher_concat ", data_personne_entraineur_afficher_concat, " Type : ",
                      type(data_personne_entraineur_afficher_concat))

                # Retourne les données du "SELECT"
                return data_personne_entraineur_afficher_concat


        except pymysql.Error as erreur:
            print(f"dgte gfadc pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"dgte gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"dgte gfadc Exception {erreur.args}")
            raise MaBdErreurConnexion(
                f"dgte gfadc Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"dgte gfadc pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def personne_entraineur_add (self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Insérer une (des) nouvelle(s) association(s) entre "id_film" et "id_genre" dans la "t_genre_film"
            # strsql_insert_type_entraineur = """INSERT INTO t_genres_films (id_genre_film, fk_genre, fk_film)
            #                                 VALUES (NULL, %(value_fk_genre)s, %(value_fk_film)s)"""
            # strsql_insert_type_entraineur = """INSERT INTO t_avoirtype (id_avoirtype, FK_type, FK_entraineur)
            #                                 VALUES (NULL, %(value_fk_type)s, %(value_fk_entraineur)s)"""
            strsql_insert_personne_entraineur = """INSERT INTO t_avoirlicencejs (id_AvoirLicenceJS, FK_Entraineur, FK_Personne)
                                                VALUES (NULL, %(value_fk_entraineur)s, %(value_fk_personne)s, NULL)"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_insert_personne_entraineur, valeurs_insertion_dictionnaire)


        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

    def personne_entraineur_delete (self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Effacer une (des) association(s) existantes entre "id_film" et "id_genre" dans la "t_genre_film"
            # strsql_delete_genre_film = """DELETE FROM t_genres_films WHERE fk_genre = %(value_fk_genre)s AND fk_film = %(value_fk_film)s"""
            # strsql_delete_type_entraineur = """DELETE FROM t_avoirtype WHERE FK_type = %(value_fk_type)s AND  FK_entraineur = %(value_fk_entraineur)s"""
            strsql_delete_personne_entraineur = """DELETE FROM t_avoirlicencejs WHERE FK_Personne = %(value_fk_personne)s AND FK_Entraineur = %(value_fk_entraineur)s"""
            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_delete_personne_entraineur, valeurs_insertion_dictionnaire)
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème type_entraineur_delete Gestions Type entraineur numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème personne_entraineur_delete Gestions personne entraineur numéro de l'erreur : {erreur}", "danger")
            raise Exception(
                "Raise exception... Problème personne_entraineur_delete Gestions personne entraineur {erreur}")

    def edit_personne_data (self, valeur_id_dictionnaire):
        try:
            print(valeur_id_dictionnaire)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le genre sélectionné dans le tableau dans le formulaire HTML
            # str_sql_id_genre = "SELECT id_genre, intitule_genre FROM t_genres WHERE id_genre = %(value_id_genre)s"
            # str_sql_id_type = "SELECT id_TypeEntraineur, Type_TypeEntraineur FROM t_typeentraineur where id_TypeEntraineur = %(value_id_type)s"
            str_sql_id_personne = "SELECT id_personne, nom_personne, prenom_personne, dateNaissance_personne FROM t_personne WHERE id_personne = %(value_id_personne)"
            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_id_personne, valeur_id_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except Exception as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème edit_personne_data Data Gestions personne numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions Genres numéro de l'erreur : {erreur}", "danger")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception... Problème edit_personne_data d'un genre Data Gestions personne {erreur}")

    def update_personne_data (self, valeur_update_dictionnaire):
        try:
            print(valeur_update_dictionnaire)
            # OM 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditIntituleGenreHTML" du form HTML "GenresEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditIntituleGenreHTML" value="{{ row.intitule_genre }}"/></td>
            # str_sql_update_intitulegenre = "UPDATE t_genres SET intitule_genre = %(value_name_genre)s WHERE id_genre = %(value_id_genre)s"
            # str_sql_update_nom__personne = "UPDATE t_typeentraineur SET Type_TypeEntraineur = %(value_name_type)s WHERE id_TypeEntraineur = %(value_id_type)s"
            str_sql_update_nom__personne = "UPDATE t_personne SET nom_personne = %(nom_personne)s, prenom_personne =%(prenom_personne)s, " \
                                           "dateNaissance_personne = %(dateNaissance_personne)s WHERE id_personne = %(id_personne)s"
            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_update_nom__personne, valeur_update_dictionnaire)

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème update_personne_data Data Gestions personne numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions Genres numéro de l'erreur : {erreur}", "danger")
            # raise Exception('Raise exception... Problème update_genre_data d\'un genre Data Gestions Genres {}'.format(str(erreur)))
            if erreur.args[0] == 1062:
                flash(f"Flash. Cette valeur existe déjà : {erreur}", "warning")
                # Deux façons de communiquer une erreur causée par l'insertion d'une valeur à double.
                flash(f"Doublon !!! Introduire une valeur différente", "warning")
                # Message en cas d'échec du bon déroulement des commandes ci-dessus.
                print(f"Problème update_personne_data Data Gestions personne numéro de l'erreur : {erreur}")

                raise Exception("Raise exception... Problème update_personne_data d'un genre DataGestionspersonne {erreur}")

    def delete_select_personne_data (self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # OM 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditIntituleGenreHTML" du form HTML "GenresEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditIntituleGenreHTML" value="{{ row.intitule_genre }}"/></td>

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le genre sélectionné dans le tableau dans le formulaire HTML
            # str_sql_select_id_genre = "SELECT id_genre, intitule_genre FROM t_genres WHERE id_genre = %(value_id_genre)s"
            str_sql_select_id_personne = "SELECT id_personne, nom_personne, prenom_personne, dateNaissance_personne FROM t_personne WHERE id_personne = %(value_id_personne)s"
            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une gméthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_select_id_personne, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_select_entraineur_data Gestions Entraineur numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème delete_select_entraineur_data numéro de l'erreur : {erreur}", "danger")
            raise Exception(
                "Raise exception... Problème delete_select_genre_data d\'un genre Data Gestions Genres {erreur}")

    def delete_personne_data(self, valeur_delete_dictionnaire):
        try:
            print("valeur dico DEBUUUUGGGGGGGGEEEEEEEEEEEERRRRRRR ", valeur_delete_dictionnaire)
            # GH 2019.04.21 Commande MySql pour EFFACER la valeur sélectionnée par le "bouton" du form HTML "entraineur_edit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            str_sql_delete_nom_personne = "DELETE FROM t_personne WHERE id_personne = %(value_id_personne)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_nom_personne, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_entraineur_data Data Gestions Personne Entraineur numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions Entraineur numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # OM 2020.04.09 Traitement spécifique de l'erreur 1451 Cannot delete or update a parent row: a foreign key constraint fails
                # en MySql le moteur INNODB empêche d'effacer un entraineur qui est associé à un type dans la table intermédiaire "t_avoirtype"
                # il y a une contrainte sur les FK de la table intermédiaire "t_avoirtype"
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                # flash(f"Flash. IMPOSSIBLE d'effacer !!! Cet entraineur est associé à des types dans la t_avoirtype !!! : {erreur}", "danger")
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(
                    f"IMPOSSIBLE d'effacer !!! Cet entraineur est associé à une dans la t_avoirlicence !!! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")


