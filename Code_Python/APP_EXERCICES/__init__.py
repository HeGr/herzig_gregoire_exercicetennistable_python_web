# __init__.py
# Pour les personnes qui veulent savoir ce que signifie __name__ une démonstration se trouve ici :
# https://www.studytonight.com/python/_name_-as-main-method-in-python
# C'est une chaîne de caractère qui permet de savoir si on exécute le code comme script principal
# appelé directement avec Python et pas importé.
from flask import Flask, render_template, flash, redirect, url_for, session, request, send_from_directory, jsonify
# from flask import Flask, flash, render_template
from Code_Python.APP_EXERCICES.DATABASE import connect_db_context_manager

# Objet qui fait "exister" notre application
obj_mon_application = Flask(__name__, template_folder="templates")
# Flask va pouvoir crypter les cookies
obj_mon_application.secret_key = '_vogonAmiral_)?^'

# GH 2020.04.21 Tout commence ici par "indiquer" les routes de l'application

# routes tables "principales"
from Code_Python.APP_EXERCICES import routes
from Code_Python.APP_EXERCICES.EXERCICE import routes_gestion_exercice
from Code_Python.APP_EXERCICES.TYPE import routes_gestion_type
from Code_Python.APP_EXERCICES.MATERIEL import routes_gestion_materiel
from Code_Python.APP_EXERCICES.PERSONNE import routes_gestion_personne
from Code_Python.APP_EXERCICES.ENTRAINEUR import routes_gestion_entraineur
# routes tables intermédiaires
from Code_Python.APP_EXERCICES.TYPE_EXERCICE import routes_gestion_type_exercice
from Code_Python.APP_EXERCICES.MATERIEL_EXERCICE import routes_gestion_materiel_exercice
from Code_Python.APP_EXERCICES.ENTRAINEUR_EXERCICE import routes_gestion_entraineur_exercice
from Code_Python.APP_EXERCICES.PERSONNE_ENTRAINEUR import routes_gestion_personne_entraineur